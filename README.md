## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## For production

```bash
$ docker-compose up --build -d
```

## Example development.env:

```bash
SMTP_TRANSPORT = smtps://user@domain.com:password@smtp.domain.com
SEND_EMAIL_FROM = "Name" <name@domain.com>

REDIS_HOST = localhost
REDIS_PORT = 6379
REDIS_DB_NUMBER = 0
REDIS_PASSWORD =
REDIS_KEY_PREFIX = tst

POSTGRES_HOST = localhost
POSTGRES_PORT = 5432
POSTGRES_DB = tst

JWT_SECRET = 'my jwt secret'
```
