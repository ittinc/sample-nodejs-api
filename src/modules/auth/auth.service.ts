import {
  Injectable,
  UnauthorizedException,
  forwardRef,
  Inject,
  UnprocessableEntityException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { RedisService } from 'nestjs-redis';
import { MailerService } from '@nest-modules/mailer';

import { UserService } from '../user/user.service';
import { User } from '../user/user.entity';
import { LoginDto } from './dto/login.dto';
import { HasherService } from '../hasher/hasher.service';
import { BlacklistService } from '../blacklist/blacklist.service';
import { UserCompact } from '../user/interfaces/userCompact.interface';

export interface Tokens {
  accessToken: string;
  refreshToken: string;
}

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    private readonly hasherService: HasherService,
    private readonly jwtService: JwtService,
    private readonly blacklistService: BlacklistService,
    private readonly redisService: RedisService,
    private readonly mailerService: MailerService,
  ) {}

  async verifyCredentials(credentials: LoginDto): Promise<User> {
    const invalidCredentialsError = 'Invalid login or password.';

    const foundUser = await this.userService.findOne({
      select: ['password', 'salt', 'id', 'email', 'verified'],
      where: [
        {
          email: credentials.login,
          deleted: false,
        },
        {
          phone: credentials.login,
          deleted: false,
        },
      ],
    });

    if (!foundUser) {
      throw new UnauthorizedException(invalidCredentialsError);
    }

    const blacklistedCount = await this.blacklistService.count({
      user: foundUser.id,
    });

    if (blacklistedCount > 0) {
      throw new UnauthorizedException(invalidCredentialsError);
    }

    const passwordMatched = await this.hasherService.comparePassword(
      credentials.password,
      foundUser.password,
      foundUser.salt,
    );

    if (!passwordMatched) {
      throw new UnauthorizedException(invalidCredentialsError);
    }

    return foundUser;
  }

  async login(user: UserCompact): Promise<Tokens> {
    const userData = await this.userService.findOne({ where: { id: user.id } });
    const payload = {
      id: user.id,
      email: userData.email,
      verified: userData.verified,
    };

    const accessToken = await this.jwtService.signAsync(payload, {
      expiresIn: '1h',
    });

    const refreshToken = await this.jwtService.signAsync(payload, {
      expiresIn: '60d',
    });

    return {
      accessToken: `Bearer ${accessToken}`,
      refreshToken: `Bearer ${refreshToken}`,
    };
  }

  async refreshToken(token: string): Promise<Tokens> {
    try {
      const blacklistedCount = await this.blacklistService.count({
        refreshToken: token,
      });

      await this.blacklistService.create({
        refreshToken: token,
        reason: 'REFRESH',
      });

      if (blacklistedCount > 0) {
        throw new UnauthorizedException('Invalid token');
      }

      const userProfile = await this.jwtService.verifyAsync(token);
      return await this.login(userProfile);
    } catch (e) {
      throw new UnauthorizedException('Invalid token');
    }
  }

  async logout(user: User, accessToken: string, refreshToken: string) {
    const userProfile = await this.jwtService.verifyAsync(refreshToken);

    if (user.id !== userProfile.id) {
      throw new UnprocessableEntityException('Invalid token');
    }

    await this.blacklistService.create({
      accessToken,
      refreshToken,
      reason: 'LOGOUT',
    });

    return { success: true };
  }

  async sendConfirmLink(id: number, email: string) {
    const token = await this.hasherService.generateRandomHash();
    console.log(token); // TODO: remove

    const client = await this.redisService.getClient();
    await client.set(token, id, 'EX', 3600 * 24); // seconds

    await this.mailerService.sendMail({
      to: email,
      subject: 'Confirm account link',
      text: 'Go to link below',
      html: `/auth/confirm/${token}. expires in 24 hours`,
    });
  }

  async approveConfirmLink(id: number, token: string) {
    const client = await this.redisService.getClient();
    const storedToken = await client.get(token);
    if (storedToken) {
      await this.userService.verifyUser(id);
    }
  }
}
