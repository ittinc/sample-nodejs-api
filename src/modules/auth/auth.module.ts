import { Module, forwardRef } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt.strategy';
import { UserModule } from '../user/user.module';
import { HasherModule } from '../hasher/hasher.module';
import { BlacklistModule } from '../blacklist/blacklist.module';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';

const jwtModuleProvider = JwtModule.registerAsync({
  imports: [ConfigModule],
  useFactory: async (configService: ConfigService) => ({
    secret: configService.get('JWT_SECRET'),
  }),
  inject: [ConfigService],
});

@Module({
  imports: [
    forwardRef(() => UserModule),
    PassportModule,
    HasherModule,
    BlacklistModule,
    jwtModuleProvider,
    ConfigModule,
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
