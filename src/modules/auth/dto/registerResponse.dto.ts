import { ApiModelProperty } from '@nestjs/swagger';

export class RegisterResponseDto {
  @ApiModelProperty()
  readonly id: number;

  @ApiModelProperty()
  readonly email: string;

  @ApiModelProperty({
    nullable: true,
  })
  readonly phone: string | null;

  @ApiModelProperty()
  readonly firstName: string;

  @ApiModelProperty({
    nullable: true,
  })
  readonly lastName: string | null;

  @ApiModelProperty({
    type: 'string',
    format: 'date-time',
  })
  readonly createdAt: Date;

  @ApiModelProperty({
    type: 'string',
    format: 'date-time',
  })
  readonly updatedAt: Date;

  @ApiModelProperty()
  readonly verified: boolean;
}
