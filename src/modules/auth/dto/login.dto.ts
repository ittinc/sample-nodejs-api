import { IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class LoginDto {
  @ApiModelProperty({
    description: 'Email or phone',
  })
  @IsNotEmpty()
  readonly login: string;

  @ApiModelProperty({
    description: 'Minimum 8 symbols',
  })
  @IsNotEmpty()
  readonly password: string;
}
