import { ApiModelProperty } from '@nestjs/swagger';

export class TokensResponseDto {
  @ApiModelProperty()
  readonly accessToken: string;

  @ApiModelProperty()
  readonly refreshToken: string;
}
