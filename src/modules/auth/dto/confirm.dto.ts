import { ApiModelProperty } from '@nestjs/swagger';

export class ConfirmDto {
  @ApiModelProperty()
  readonly token: string;
}
