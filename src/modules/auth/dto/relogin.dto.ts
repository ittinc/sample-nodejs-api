import { ApiModelProperty } from '@nestjs/swagger';

export class ReloginDto {
  @ApiModelProperty()
  readonly refreshToken: string;
}
