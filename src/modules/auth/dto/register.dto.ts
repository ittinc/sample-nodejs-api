import {
  IsEmail,
  IsPhoneNumber,
  IsNotEmpty,
  IsOptional,
} from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class RegisterDto {
  @ApiModelProperty()
  @IsEmail()
  readonly email: string;

  @ApiModelProperty({
    required: false,
  })
  @IsOptional()
  @IsPhoneNumber('ZZ')
  readonly phone?: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly firstName: string;

  @ApiModelProperty({
    required: false,
  })
  @IsOptional()
  readonly lastName?: string;

  @ApiModelProperty({
    description: 'Minimum 8 symbols',
  })
  @IsNotEmpty()
  readonly password: string;
}
