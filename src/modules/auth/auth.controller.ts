import {
  Controller,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  Request,
  Get,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { ExtractJwt } from 'passport-jwt';

import { RegisterDto } from './dto/register.dto';
import { RegisterResponseDto } from './dto/registerResponse.dto';

import { LoginDto } from './dto/login.dto';
import { TokensResponseDto } from './dto/tokensResponse.dto';

import { ConfirmDto } from './dto/confirm.dto';
import { ReloginDto } from './dto/relogin.dto';

import { AuthService } from './auth.service';

import { UserService } from '../user/user.service';
import { SuccessDto } from '../../dto/success.dto';
import { CustomRequest } from '../../interfaces/customRequest.interface';

const divideToken = (token: string) => token.split(' ')[1];

@ApiUseTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
  ) {}

  @Post('/')
  @ApiResponse({
    status: 200,
    description: 'Create user.',
    type: RegisterResponseDto,
  })
  async create(
    @Body() userRegisterDto: RegisterDto,
  ): Promise<RegisterResponseDto> {
    const user = await this.userService.create(userRegisterDto);
    this.authService.sendConfirmLink(user.id, user.email).catch();
    return user;
  }

  @Post('/send-confirm-link')
  @ApiResponse({
    status: 200,
    description: 'Send confirm link.',
    type: SuccessDto,
  })
  @UseGuards(AuthGuard('jwt'))
  async sendConfirmLink(@Request() req: CustomRequest): Promise<SuccessDto> {
    if (!req.user.verified) {
      this.authService.sendConfirmLink(req.user.id, req.user.email).catch();
    }

    return { success: true };
  }

  @Post('/confirm')
  @ApiResponse({
    status: 200,
    description: 'Verify user.',
    type: SuccessDto,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async approveConfirmLink(
    @Request() req: CustomRequest,
    @Body() params: ConfirmDto,
  ): Promise<SuccessDto> {
    this.authService.approveConfirmLink(req.user.id, params.token).catch();
    return { success: true };
  }

  @Post('login')
  @ApiResponse({
    status: 200,
    description: 'Get tokens.',
    type: TokensResponseDto,
  })
  @UseGuards(AuthGuard('local'))
  async login(
    @Request() req: CustomRequest,
    @Body() body: LoginDto,
  ): Promise<TokensResponseDto> {
    return this.authService.login(req.user);
  }

  @Post('relogin')
  @ApiResponse({
    status: 200,
    description: 'Update tokens by refresh token.',
    type: TokensResponseDto,
  })
  async relogin(@Body() params: ReloginDto): Promise<TokensResponseDto> {
    return this.authService.refreshToken(divideToken(params.refreshToken));
  }

  @Delete(':refreshToken')
  @ApiResponse({
    status: 200,
    description: 'Logout user.',
    type: SuccessDto,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async logout(
    @Request() req,
    @Param() params: ReloginDto,
  ): Promise<SuccessDto> {
    const accessToken = ExtractJwt.fromAuthHeaderAsBearerToken()(req);
    return this.authService.logout(
      req.user,
      accessToken,
      divideToken(params.refreshToken),
    );
  }

  @Get('me')
  @ApiResponse({
    status: 200,
    description: 'Get user info.',
    type: RegisterResponseDto,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  async me(@Request() req: CustomRequest): Promise<RegisterResponseDto> {
    return await this.userService.findOne({ where: { id: req.user.id } });
  }
}
