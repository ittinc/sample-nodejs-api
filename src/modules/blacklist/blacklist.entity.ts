import {
  Entity,
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';

@Entity()
export class Blacklist {
  @ApiModelProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiModelProperty()
  @Column('int', { nullable: true })
  user?: number;

  @ApiModelProperty()
  @Column('text', { nullable: true })
  accessToken?: string;

  @ApiModelProperty()
  @Column('text', { nullable: true })
  refreshToken?: string;

  @ApiModelProperty()
  @Column('text')
  reason: string;

  @Column('date')
  @CreateDateColumn()
  createdAt: Date;
}
