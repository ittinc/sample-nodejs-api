import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindConditions } from 'typeorm';

import { Blacklist } from './blacklist.entity';
import { validate } from 'class-validator';

interface BlacklistCreateParams {
  readonly accessToken?: string;
  readonly refreshToken?: string;
  readonly user?: number;
  readonly reason: string;
}

@Injectable()
export class BlacklistService {
  constructor(
    @InjectRepository(Blacklist)
    private readonly blacklistRepository: Repository<Blacklist>,
  ) {}

  async count(params: FindConditions<Blacklist>) {
    return await this.blacklistRepository.count(params);
  }

  async create(params: BlacklistCreateParams) {
    const blacklist = new Blacklist();
    Object.assign(blacklist, params);
    await validate(blacklist);

    return await this.blacklistRepository.save(blacklist);
  }
}
