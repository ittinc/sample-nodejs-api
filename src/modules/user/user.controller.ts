import {
  Controller,
  Post,
  Body,
  UnprocessableEntityException,
  ServiceUnavailableException,
} from '@nestjs/common';
import { RedisService } from 'nestjs-redis';
import { MailerService } from '@nest-modules/mailer';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

import { PasswordRecoveryDto } from './dto/passwordRecovery.dto';
import { PasswordResetDto } from './dto/passwordReset.dto';
import { SuccessDto } from '../../dto/success.dto';

import { UserService } from './user.service';
import { HasherService } from '../hasher/hasher.service';

@ApiUseTags('User')
@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly hasherService: HasherService,
    private readonly mailerService: MailerService,
    private readonly redisService: RedisService,
  ) {}

  @Post('password/forgot')
  @ApiResponse({
    status: 200,
    description: 'Send reset link.',
    type: SuccessDto,
  })
  async passwordForgot(
    @Body() params: PasswordRecoveryDto,
  ): Promise<SuccessDto> {
    const token = await this.hasherService.generateRandomHash();
    console.log(token); // TODO: remove

    const client = await this.redisService.getClient();

    const user = await this.userService.findOne({
      where: { email: params.email },
    });

    if (!user) {
      throw new UnprocessableEntityException('User not found');
    }

    await client.set(token, user.id.toString(), 'EX', 3600); // seconds

    this.mailerService
      .sendMail({
        to: user.email,
        subject: 'Reset password link',
        text: 'Go to link below',
        html: `/auth/forgot/reset/${token}. expires in one hour`,
      })
      .catch();

    return { success: true };
  }

  @Post('password/reset')
  @ApiResponse({
    status: 200,
    description: 'Reset password.',
    type: SuccessDto,
  })
  async passwordReset(@Body() body: PasswordResetDto): Promise<SuccessDto> {
    const client = await this.redisService.getClient();

    const id = await client.get(body.token);

    if (!id) {
      throw new UnprocessableEntityException('Token not found');
    }

    const user = await this.userService.findOne({
      where: { id, deleted: false },
    });

    if (!user) {
      throw new UnprocessableEntityException('User not found');
    }

    try {
      await this.userService.updatePassword(parseInt(id, 10), body.password);
    } catch (e) {
      throw new ServiceUnavailableException('Password service is down');
    }

    await client.del(body.token);

    return { success: true };
  }
}
