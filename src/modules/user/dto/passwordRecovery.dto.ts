import { IsEmail } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class PasswordRecoveryDto {
  @ApiModelProperty()
  @IsEmail()
  readonly email: string;
}
