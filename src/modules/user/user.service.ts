import * as R from 'ramda';
import {
  Injectable,
  ConflictException,
  ServiceUnavailableException,
} from '@nestjs/common';
import { Repository, FindOneOptions } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { validate } from 'class-validator';

import { User } from './user.entity';
import { UserRO } from './interfaces/userRO.interface';

import { RegisterDto } from '../auth/dto/register.dto';
import { AuthService, Tokens } from '../auth/auth.service';
import { HasherService } from '../hasher/hasher.service';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly hasherService: HasherService,
    private readonly authService: AuthService,
  ) {}

  async findOne(params: FindOneOptions): Promise<User> {
    return await this.userRepository.findOne(params);
  }

  async create(user: RegisterDto): Promise<UserRO> {
    try {
      const { hash, salt } = await this.hasherService.hashPassword(
        user.password,
      );

      const newUser = new User({
        ...user,
        salt,
        password: hash,
      });

      await validate(newUser);

      const savedUser = await this.userRepository.save(newUser);
      const tokens = await this.authService.login(savedUser);
      return this.buildUserRO(R.merge(savedUser, { tokens }));
    } catch (error) {
      if (error.code === '23505' && error.detail.includes('email')) {
        throw new ConflictException('Email is already taken');
      } else if (error.code === '23505' && error.detail.includes('phone')) {
        throw new ConflictException('Phone is already taken');
      } else {
        throw new ServiceUnavailableException('Can not save user');
      }
    }
  }

  async updatePassword(id: number, plainPassword: string) {
    const { hash, salt } = await this.hasherService.hashPassword(plainPassword);
    await this.userRepository.update({ id }, { password: hash, salt });
  }

  async verifyUser(id: number) {
    await this.userRepository.update({ id }, { verified: true });
  }

  private buildUserRO(user: User & { tokens?: Tokens }): UserRO {
    return R.omit(['password', 'salt', 'deleted', 'deletedAt'], user);
  }
}
