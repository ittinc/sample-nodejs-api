import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Unique,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
@Unique(['email'])
@Unique(['phone'])
export class User {
  constructor(user: Partial<User>) {
    Object.assign(this, user);
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 255 })
  email: string;

  @Column({ length: 20, nullable: true })
  phone: string;

  @Column({ select: false })
  password: string;

  @Column('text', { select: false })
  salt: string;

  @Column({ length: 128 })
  firstName: string;

  @Column({ length: 128, nullable: true })
  lastName: string;

  @Column({ default: false })
  verified: boolean;

  @Column('bool', { default: false, select: false })
  deleted: boolean;

  @Column('date')
  @CreateDateColumn()
  createdAt: Date;

  @Column('date')
  @UpdateDateColumn()
  updatedAt: Date;

  @Column('date')
  @CreateDateColumn({ default: null, select: false })
  deletedAt: Date | null;
}
