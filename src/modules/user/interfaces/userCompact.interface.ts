export interface UserCompact {
  id: number;
  email: string;
  phone: string;
  verified: boolean;
}
