export interface UserRO {
  id: number;
  email: string;
  phone: string;
  firstName: string;
  lastName: string;
  createdAt: Date;
  updatedAt: Date;
  verified: boolean;
}
