export interface Pkb2fConfig {
  iterations: number;
  keylen: number;
  digestAlgorithm: string;
  encoding: string;
  salten: number;
}

export const hasherProvider = {
  provide: 'PKB2F_CONFIG',
  useValue: {
    iterations: 25000,
    keylen: 512,
    digestAlgorithm: 'sha256',
    encoding: 'hex',
    salten: 32,
  } as Pkb2fConfig,
};
