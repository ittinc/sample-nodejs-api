import { Injectable, Inject } from '@nestjs/common';
import * as crypto from 'crypto';
import { Pkb2fConfig } from './hasher.provider';

interface Hasher {
  hashPassword(password: string, salt?: string): Promise<PasswordChain>;
  comparePassword(
    providedPass: string,
    storedPass: string,
    storedSalt: string,
  ): Promise<boolean>;
}

interface PasswordChain {
  hash: string;
  salt: string;
}

@Injectable()
export class HasherService implements Hasher {
  constructor(
    @Inject('PKB2F_CONFIG') private readonly pkb2fConfigProvider: Pkb2fConfig,
  ) {}

  private async hashBySalt(plain: string, salt: string) {
    return new Promise<string>((resolve, reject) => {
      crypto.pbkdf2(
        plain,
        salt,
        this.pkb2fConfigProvider.iterations,
        this.pkb2fConfigProvider.keylen,
        this.pkb2fConfigProvider.digestAlgorithm,
        (error: Error | null, derivedKey: Buffer) => {
          return error
            ? reject(error)
            : resolve(derivedKey.toString(this.pkb2fConfigProvider.encoding));
        },
      );
    });
  }

  private async generateSalt() {
    return new Promise<string>((resolve, reject) => {
      crypto.randomBytes(
        this.pkb2fConfigProvider.salten,
        (error: Error | null, raw: Buffer) => {
          return error
            ? reject(error)
            : resolve(raw.toString(this.pkb2fConfigProvider.encoding));
        },
      );
    });
  }

  public async hashPassword(
    password: string,
    salt?: string,
  ): Promise<PasswordChain> {
    if (!salt) {
      salt = await this.generateSalt();
    }

    const hash = await this.hashBySalt(password, salt);

    return {
      hash,
      salt,
    };
  }

  public async comparePassword(
    providedPass: string,
    storedPass: string,
    storedSalt: string,
  ): Promise<boolean> {
    const { hash } = await this.hashPassword(providedPass, storedSalt);
    return hash === storedPass;
  }

  public async generateRandomHash(): Promise<string> {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(20, (error: Error, buffer: Buffer) => {
        return error ? reject(error) : resolve(buffer.toString('hex'));
      });
    });
  }
}
