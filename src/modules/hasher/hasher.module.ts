import { Module } from '@nestjs/common';
import { HasherService } from './hasher.service';
import { hasherProvider } from './hasher.provider';

@Module({
  providers: [HasherService, hasherProvider],
  exports: [HasherService],
})
export class HasherModule {}
