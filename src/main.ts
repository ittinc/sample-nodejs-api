import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';
import * as RedisStore from 'rate-limit-redis';
import 'reflect-metadata';

import { AppModule } from './app.module';
import { RedisService } from 'nestjs-redis';

async function bootstrap() {
  const cors =
    process.env.NODE_ENV === 'development'
      ? true
      : {
          origin: 'http://demo.domain.com',
          optionsSuccessStatus: 200,
        };

  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({ logger: false }),
    {
      cors,
    },
  );

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );

  const redisService = app.get(RedisService);
  const redisClient = redisService.getClient();

  app.use(helmet());
  app.use(
    rateLimit({
      store: new RedisStore({
        client: redisClient,
      }),
      windowMs: 3 * 60 * 1000,
      max: 100,
      headers: false,
    }),
  );

  app.setGlobalPrefix('api');

  const options = new DocumentBuilder()
    .setTitle('Api sample')
    .setDescription('')
    .setBasePath('/api')
    .setVersion('0.1.0')
    .addTag('LA')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);

  await app.listen(3000, '0.0.0.0');
}

bootstrap();
