import { FastifyRequest } from 'fastify';
import { UserCompact } from '../modules/user/interfaces/userCompact.interface';

export interface CustomRequest extends FastifyRequest {
  user: UserCompact;
}
