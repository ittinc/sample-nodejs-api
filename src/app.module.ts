import { Module } from '@nestjs/common';
import { RedisModule } from 'nestjs-redis';
import { HandlebarsAdapter, MailerModule } from '@nest-modules/mailer';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from './modules/config/config.service';
import { ConfigModule } from './modules/config/config.module';
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './modules/auth/auth.module';

const databaseModuleProvider = TypeOrmModule.forRootAsync({
  imports: [ConfigModule],
  useFactory: async (configService: ConfigService) => ({
    type: 'postgres' as 'postgres',
    host: configService.get('POSTGRES_HOST'),
    username: configService.get('POSTGRES_USER'),
    password: configService.get('POSTGRES_PASSWORD'),
    port: parseInt(configService.get('POSTGRES_PORT'), 10),
    database: configService.get('POSTGRES_DB'),
    entities: [__dirname + '/**/**.entity{.ts,.js}'],
    synchronize: true,
  }),
  inject: [ConfigService],
});

const redisModuleProvider = RedisModule.forRootAsync({
  imports: [ConfigModule],
  useFactory: async (configService: ConfigService) => ({
    host: configService.get('REDIS_HOST'),
    port: parseInt(configService.get('REDIS_PORT'), 10),
    db: parseInt(configService.get('REDIS_DB_NUMBER'), 10),
    password: configService.get('REDIS_PASSWORD'),
    keyPrefix: configService.get('REDIS_KEY_PREFIX'),
  }),
  inject: [ConfigService],
});

const mailerModuleProvider = MailerModule.forRootAsync({
  imports: [ConfigModule],
  useFactory: (configService: ConfigService) => ({
    transport: configService.get('SMTP_TRANSPORT'),
    defaults: {
      from: configService.get('SEND_EMAIL_FROM'),
    },
    template: {
      dir: __dirname + '/templates',
      adapter: new HandlebarsAdapter(),
      options: {
        strict: true,
      },
    },
  }),
  inject: [ConfigService],
});

@Module({
  imports: [
    databaseModuleProvider,
    redisModuleProvider,
    mailerModuleProvider,
    AuthModule,
    UserModule,
    ConfigModule,
  ],
})
export class AppModule {}
