import { ApiModelProperty } from '@nestjs/swagger';

export class SuccessDto {
  @ApiModelProperty()
  readonly success: boolean = true;
}
