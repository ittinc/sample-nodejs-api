const fs = require('fs');
const process = require('process');
const dotenv = require('dotenv');

const filePath = `${process.env.NODE_ENV}.env`;
const configService = dotenv.parse(fs.readFileSync(filePath));

module.exports = {
  type: 'postgres',
  host: configService['POSTGRES_HOST'],
  port: parseInt(configService['POSTGRES_PORT'], 10),
  database: configService['POSTGRES_DB'],
  entities: [__dirname + '/**/**.entity{.ts,.js}'],
  synchronize: true,
  dropSchema: false,
  logging: true,
  migrations: ['migrations/**/*.ts'],
  subscribers: ['subscriber/**/*.ts', 'dist/subscriber/**/.js'],
  cli: {
    entitiesDir: 'src',
    migrationsDir: 'migrations',
    subscribersDir: 'subscriber',
  },
};
